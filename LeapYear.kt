fun main(args:Array<String>){
	val year:Int;
	try {
		year = Integer.parseInt(args[0]);
		when {
			year % 400 == 0 -> println("Leap Year");
			year % 4 == 0 && year % 100 != 0 -> println("Leap Year");
			else -> println("Not a 	Leap Year");
		}
	} catch(e : NumberFormatException){
		println("Enter a Valid Year");
	}
}
